﻿jQuery(document).ready(function($){
 // "use strict";
  var is_tablet, is_smartphone = false;
  var stop_animation = false;
  var hovering_nav = false;
  var scrolling_programatically = false;
  /* Resizes the slider to fit the current window's size */
  function sliderResize(){
    // Reset the vertical centering
    $('.mainslider nav li .title').css('padding-top', 0).css('padding-bottom', 0);    
    $('.mainslider nav li').each(function(){
      var slide = $(this);
      var slide_height = slide.outerHeight();
      var slide_container_height = slide.parent().outerHeight();
      
      if(slide_height < slide_container_height) {
        var difference = slide_container_height - slide_height;
        slide.find('.title').css('padding-top', difference/2).css('padding-bottom', difference/2);
      }   
      var slide_width = slide.parent().parent().width() / 4;
      slide.css('width', slide_width);
      slide.parent().css('width', slide_width * $('.mainslider nav li').length + ((!!window.ActiveXObject || "ActiveXObject" in window) ? slide_width : 0));   
      var aspect_ratio = jQuery(window).height() / jQuery(window).width();
      var thumb_height = jQuery('.mainslider nav li .thumbnail').width() * aspect_ratio * 0.8;
      jQuery('.mainslider nav li .thumbnail').css( { 
        'height': thumb_height,
        'top': -(thumb_height - 14)
      });
    });
    
    var more_button_caption_height = $('.mainslider nav .more .title').height();
    $('.mainslider nav .more .title').css('margin-top', - (more_button_caption_height/2) ).addClass('loaded');
  
  }
  
  /* Animates the slider to the next item */
  function sliderAnimateNextItem(){
    if(!stop_animation){
      // Variables
      var wrapper_width = jQuery(".mainslider nav .nav").width();
      var inner_width = jQuery(".mainslider nav ul").width();
      var top_right_offset = -(inner_width - wrapper_width);
      $('.mainslider nav ul li.next-item').find('.progress').animate({
        'width': '100%'
      }, 5000, 'linear', function(){
        if(!stop_animation){
          // When the animation completes,
          // next-item becomes current-item, and current-item becomes class-less
          $('.mainslider nav ul li.current-item').removeClass('current-item').find('.progress').attr('style', '');
          $(this).parent().parent().removeClass('next-item').addClass('current-item');
          
          // Remove the current-item state from the current item
          $('.slides ul li.current-item').removeClass('current-item');
          
          // Mark the next item as the current item
          var current_slide = $('.slides ul li:nth-child(' + ($(this).parent().parent().index()+1) +')');
          current_slide.addClass('current-item');
          
          // Animate the navigation to the current item (if the mouse is not over the navigation, and the navigation is not stopped)
          if(!stop_animation && !hovering_nav && $('.mainslider nav ul li').length > 4){
            var move_to = - (current_slide.index() * $('.mainslider nav ul li.current-item').outerWidth());
            if( move_to < top_right_offset){
              move_to = top_right_offset;
            }
            
            var navigation_current_offset = 0;
            if(!is_tablet){
              navigation_current_offset = parseInt($(".mainslider nav ul").css('left'), 10);
            }else{
              navigation_current_offset = parseInt($(".mainslider .nav").scrollLeft(), 10);
            }
            
            if(isNaN(navigation_current_offset)) { navigation_current_offset = 0; }
            
            if(move_to !== navigation_current_offset){
              if(!is_tablet){
                $(".mainslider nav ul").animate({
                  'left' : move_to-1
                });
              }else{
                scrolling_programatically = true;
                $(".mainslider .nav").animate({
                  'scrollLeft' : -(move_to-1)
                }, function(){
                  setTimeout(function(){
                    scrolling_programatically = false;
                  }, 100);
                });
              }
              
            } 
          }
          
          var next_item = $(this).parent().parent().next();
          if(next_item.length > 0){
            next_item.addClass('next-item');
          }else{
            $('.mainslider nav ul li:first-child').addClass('next-item');
          }
          // Animates to the next slide
          sliderAnimateNextItem();
        }
      });
    }
  } // ends sliderAnimateNextItem()
  
  /* Initializes the slider animation */
  function sliderAnimate(){

    $(".mainslider .nav").scroll(function(){
      if(!scrolling_programatically){
        $(".mainslider .nav").addClass('animation-stopped');
        stop_animation = true;
      }
    });
    
    var slides = $('.mainslider nav ul li');
    if(slides.length > 0){
      // If there's at least one slide.
      $('.mainslider nav ul li:first-child').addClass('current-item').next().addClass('next-item');
      $('.slides ul li:first-child').addClass('current-item');

      if($('.mainslider nav ul li.next-item').length > 0){
        // If there is a next item
        // Animates to the next slide 
        sliderAnimateNextItem();
        
      }
      
      // Handle click on navigation items
      $('.mainslider nav ul li').on('click', function(){
        stop_animation = true;
        $('.mainslider nav ul li').removeClass('current-item next-item').find('.progress').css('width', 0).stop();
        $(this).addClass('current-item');
        
        // Hide the current-slide
        $('.slides ul li.current-item').removeClass('current-item');
        // Set the new current slide
        var current_slide = $('.slides ul li:nth-child(' + ($(this).index()+1) +')').addClass('current-item');
      });

    }
  } // ends sliderAnimate()
  
  /* Moves the work navigation so that it is aligned with the site grid */
  function sliderMoveToClosestItem(){
    var item_width = jQuery(".mainslider nav .more").width();
    
    var navigation_current_offset = 0;
    if(!is_tablet){
      navigation_current_offset = parseInt($(".mainslider nav ul").css('left'), 10);
    }else{
      navigation_current_offset = parseInt($(".mainslider .nav").scrollLeft(), 10);
    }
    
    var before = Math.ceil(navigation_current_offset / item_width) * item_width;
    var after = Math.floor(navigation_current_offset / item_width) * item_width;
    var move_to = after;
    
    if(Math.abs(Math.abs(navigation_current_offset) + before) <= Math.abs(Math.abs(navigation_current_offset) + after)){
      move_to = before;
    }
    
    if(!is_tablet){
      $(".mainslider nav ul").animate({
        'left' : move_to-1
      });
    }else{
      $(".mainslider .nav").animate({
        'scrollLeft' : -(move_to-1)
      });
    }
  } // ends sliderMoveToClosestItem()
  
  /* Controls the behavior of the works navigation (makes it scroll on mouse move) */
  function sliderScrollOnHover(){
    if($('.mainslider nav ul li').length > 4 && !is_tablet){
      // Variables
      var item_width = $(".mainslider nav .more").width();
      var wrapper_width = $(".mainslider nav .nav").width();
      var inner_width = $(".mainslider nav ul").width();
      var top_right_offset = -(inner_width - wrapper_width);
    
      // Handle navigation items scroll on hover
      $(".mainslider nav .nav").off('mousemove');
      $(".mainslider nav .nav").on('mousemove', function(e){
        var mouse_pos_ul = (e.pageX - item_width);
        var new_wrapper_width = wrapper_width * 0.5;
        var mouse_pos_x = mouse_pos_ul - wrapper_width * 0.25;
        var offset_percentage = (mouse_pos_x * 100) / new_wrapper_width;
        
        if(offset_percentage < 0) {
          offset_percentage = 0;
        }else if(offset_percentage > 100) {
          offset_percentage = 100;
        }

        jQuery(".mainslider nav ul").css('left', top_right_offset * (offset_percentage / 100));
        
      });
      $(".mainslider nav .nav").off('mouseenter');
      $(".mainslider nav .nav").on('mouseenter', function(){ $(".mainslider nav ul").stop(); hovering_nav = true; });
      $(".mainslider nav .nav").off('mouseleave');
      $(".mainslider nav .nav").on('mouseleave', function(){ hovering_nav = false; sliderMoveToClosestItem(); });
    }
    
  } // ends sliderScrollOnHover()
  /* Initializes the slider */
  function mainSliderInitialize(){
      
    $(window).resize(function(){
      // Recalculate slider
      sliderResize();
      // Restart scroll on hover
      sliderScrollOnHover();
    });
    
    // Resizes the images and navigation of the main slider
    sliderResize();
    
    // Animates the slider pagination
    sliderAnimate();
    
    // Scrolls the slider animation on hover
    sliderScrollOnHover();
      
  } // mainSliderInitialize()
  var window_width = $(window).width();

	mainSliderInitialize();
})(jQuery);


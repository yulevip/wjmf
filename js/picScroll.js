var imgRun = function(a, b, c, d) {// 容器选择器, 左箭头选择器, 右箭头选择器, 滚动间隔(毫秒)
	var $imgUl = $(a).find('ul:visible');
	var $imgLi = $(a).find('li');
	var divWidth = $imgUl.parent().width();
	var liWidth = $imgLi.outerWidth(true);
	var ulWidth = $imgLi.length * liWidth ;
	var $s = 0;
	var $timer = null;
	var speed = parseInt(d);
	$imgLi.clone(true).appendTo($imgUl);
	function toLeft() {
		$s -= liWidth;
		if ($s < (divWidth - ulWidth)*2) {
			$s += ulWidth;
		}
		if ($s > 0) {
			$s -= ulWidth;
		}
		$imgUl.animate({left: $s+'px'}, 300);
	}
	function toRight() {
		$s += liWidth;
		if ($s > 0) {
			$s -= ulWidth;
		}
		$imgUl.animate({left: $s+'px'}, 300);
	}
	$timer = setInterval(toLeft, speed);
	$imgUl.hover(function() {
		clearInterval($timer);
	}, function() {
		$timer = setInterval(toLeft, speed);
	});
	$(b).click(function() {
		clearInterval($timer);
    toLeft();
		$timer = setInterval(toLeft, speed);
  });
	$(c).click(function() {
		clearInterval($timer);
    toRight();
		$timer = setInterval(toLeft, speed);
  });
}

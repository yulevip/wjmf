﻿// about packer
$(document).ready(function(){

	$('#nav .space,#nav .goods').hover(function(){
		$('#'+ $(this).attr('class') +'Box').fadeIn('slow');
	},function(){
		$('#'+ $(this).attr('class') +'Box').fadeOut('slow');
	});

	gridResize();
	
	$('#location .spaceTab ul li').click(function(e) {
    $('.spaceTab span').html($(this).html());
  });

	$('#optBox li i').click(function(e) {
		var	 _this = $(this),
				 _input = _this.find('input:checkbox'),
				 _li = _this.parent(),
				 s = '';
		if(!_li.hasClass('isMulti')){
			_this.addClass('on').siblings().removeClass('on');
			_li.find('input').prop('checked', '');
			_input.prop('checked', 'true');
		}else{
			_this.toggleClass('on');
			_input.prop('checked', !_input.prop('checked'));
		}

		_li.find('input:checked').each(function(index, element) {
      s += $(this).parent().text()+', ';
    });
		if(s == ''){
			$('#searchOpt s:eq('+_li.index()+')').html('').hide();
		}else{
			$('#searchOpt s:eq('+_li.index()+')').html(_li.find('span').text()+'<u class="red">'+s+'</u>').show();
		}
  });
	
	$('#optBox li div').click(function(e) {
		var _this = $(this),
				_li = _this.parent();
		_li.find('i:eq(0)').toggleClass('on').toggle();
		_li.toggleClass('isMulti').find('input:checkbox').toggle();
    _this.addClass('hide').siblings('div').removeClass('hide');
  });
	
	$('#optBox li button').click(function(e) {
		var _li = $(this).parent(),
			val1 = _li.find('input:text:eq(0)').val(),
			val2 = _li.find('input:text:eq(1)').val();
		if(val1 == '' || val2 == ''){
			alert('请输入价格区间!');
		}else if(val1 > val2){
			alert('请输入正确的价格区间!');
		}else{
			_li.find('i').removeClass('on');
			$('#searchOpt s:eq('+_li.index()+')').html(_li.find('span').text()+'<u class="red">'+val1+' - '+val2+''+'</u>').show();
		}
		$(this).prop('checked', !$(this).prop('checked'));
	});
	
	$('#optBox li i input:checkbox').click(function(e) {
		$(this).prop('checked', !$(this).prop('checked'));
	});
	
	$('#searchOpt s').click(function(e) {
		var _li = $('#optBox li:eq('+$(this).index()+')');
		if(_li.hasClass('isMulti')){
			_li.find('div:visible').click();
		}
		_li.find('input:checkbox').prop('checked', false);
		_li.find('i:eq(0)').addClass('on').siblings().removeClass('on');
    $(this).html('').hide();
  });

	$('.showIcoU').click(function(e) {
		$(this).addClass('on').siblings().removeClass('on');
		gridSize('U');
  });
	$('.showIcoE').click(function(e) {
		$(this).addClass('on').siblings().removeClass('on');
		gridSize('E');
  });
	$('.sortBtn li').click(function(e) {
		$(this).addClass('on').siblings().removeClass('on');
		var tem = $(this).attr('data-sort').split(',');
		gridSort(tem[0], tem[1]);
  });

});


function gridResize(){
	var defaultRatio = $('#packer').attr('ratio') || 1;
	$('#packer .item:visible').each(function() {
		var t = $(this),ratio = defaultRatio;
		if(t.hasClass('w3')){ ratio/=3; }
		if(t.hasClass('h3')){ ratio*=3; }
		if(t.hasClass('w2')){ ratio/=2; }
		if(t.hasClass('h2')){ ratio*=2; }
		t.height( t.width() * ratio );
	});

	$('#packer .container').packery({
		itemSelector: '.item'
	});
}


function gridSort(by, sc){
	var arr = [];
	$('#packer .item').each(function() {
		var num = $(this).attr(by);
    arr[num] = this.outerHTML;
  });
	if(sc == 'desc'){	arr.reverse();}
	for(var i=0; i<=arr.length; i++){
		var _item = $(arr[i]),
			_itemEq = $('#packer .item:eq('+i+')');
			_itemEq.removeClass(_itemEq.attr('temclass')).attr({
				temclass : _item.attr('temclass'),
				price : _item.attr('price'),
				time : _item.attr('time')
		}).html(_item.html()).css('background-image', _item.css('background-image'));
		if($('.showIcoU').hasClass('on')){
			_itemEq.addClass(_item.attr('temclass'));
		}
	}
	gridResize();
}

function gridSize(type){
	if(type == 'U'){
		$('#packer .item').each(function() {
			$(this).addClass($(this).attr('temclass'));
		});
	}else{
		$('#packer .item').each(function() {
			$(this).removeClass($(this).attr('temclass'));
		});
	}
	gridResize();
}

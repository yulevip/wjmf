﻿// 全局js文件

$(document).ready(function(){
	//内页 导航条 选风格
	$('#nav .style').hover(function(){
		$('#stylesBox').fadeIn('slow');
	},function(){
		$('#stylesBox').fadeOut('slow');
	});
	$('#stylesBox li').click(function(e) {
    $(this).addClass('on').siblings().removeClass('on');
  });

	//鼠标放在空间和商品分类box上的效果
	$('#spaceBox>li, #goodsBox>li').hover(function(){
		$(this).find('.hideList').fadeIn();
	},function(){
		$(this).find('.hideList').fadeOut();
	});
	
	//城市选项卡切换
	$('.cityNav li').click(function(e) {
		var obj=$(this);
		tabDiv(obj,obj.index());
  });
	$('.cityList li').click(function(){
		$('#choosedCity').html($(this).html());
		$('#cityBox').fadeOut('slow');
	});
	
	//鼠标放到二维码图片上的效果
	$('.lastCol img:gt(0)').mouseover(function(e) {
    $('#QRcode').fadeTo('slow',0.3).attr('src', $(this).attr('data-qr')).fadeTo('slow',1);
  });
	
	//购物车显示/隐藏 (全局)
	/*$('.shopIcon img').click(function(e) {
    $('.simpleCart_items').fadeToggle();
  });*/
	$('.shopIcon').hover(function(e){
		$('.shopList').fadeIn('slow');
	},function(e){
		$('.shopList').fadeOut('slow');
	});

});


//city选择
function tabDiv(obj,num){
 obj.addClass('on').siblings().removeClass('on');
 $('.cityList ul:eq('+num+')').show().siblings().hide();
}